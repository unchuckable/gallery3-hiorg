<?php defined("SYSPATH") or die("No direct script access.");

    class hiorg_Controller extends Controller {

		const ALLOW_PRIVATE_GALLERY = true;
  /**
   * default action for the openid controller (i.e; gallery/openid/)
   */
    public function index() {
        print "HiOrg SSO Login Endpoint";
    }

    public function login() {
        $ov = module::get_var("hiorg", "org" );  // Organisations-Kuerzel Ihres HiOrg-Server-Accounts
        
        $userinfo = "name,vorname,username,email,perms"; // Fordert Name und Vorname an (Komma wird unten URL-encodiert)
        
        $myfqdn = empty($_SERVER["HTTPS"]) ? "http://" : "https://";
        $myfqdn.= $_SERVER["HTTP_HOST"].preg_replace("/login$/","verify",$_SERVER['PHP_SELF']);
        
        $hiorgurl = "https://www.hiorg-server.de/logmein.php";
        
        $ziel = $hiorgurl."?ov=$ov&weiter=".urlencode($myfqdn);
        
        if(!empty($userinfo)) $ziel.= "&getuserinfo=".urlencode($userinfo);
        
        Kohana_Log::add("information", "[HiOrg SSO Module] HiOrg URL = {$ziel}" );

	url::redirect( $ziel );        
    }
    
    
    public function verify() {
        $token = $_GET["token"];
        $hiorgurl = "https://www.hiorg-server.de/logmein.php";
        $completeUrl = $hiorgurl."?token=".urlencode($token);

        $data = file_get_contents( $completeUrl );
        
        if(mb_substr( $data ,0,2) != "OK") die("Login fehlgeschlagen!");
        
        $userdata = unserialize(base64_decode(mb_substr( $data , 3)));
        
        $valid = hiorg::process_user( $userdata );
        
        if ( $valid ) {

//            $continueUrl = Session::instance()->get( "continue_url" );
//            if (!empty($continueUrl) ) {
//                header( "Location: ".$continueUrl );
//                exit;
//            }
        
            header("Location: http://linuxkings.de/~kater/gallery3/index.php" );
            exit;
        }
        
    }
        
    public function test() {
        hiorg::testCall();
    }
        
    public function reset() {
        module::set_var("hiorg","org","nii");
        module::set_var("hiorg","logDebugInfo",TRUE);
    }
}
?>
